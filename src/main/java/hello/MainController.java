package hello;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class MainController {

    @RequestMapping("/")
    public @ResponseBody String hello() {
        return "Hello Controller v3";
    }
    
    @RequestMapping("/toto")
    public @ResponseBody String toto() {
        return "Hello it's Toto !";
    }
}
