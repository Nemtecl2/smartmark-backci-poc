FROM openjdk:latest
MAINTAINER Meher Hedhli
VOLUME /tmp
ADD /target/backci-poc-1.0.jar app.jar

RUN sh -c 'touch /app.jar'
CMD ["java", "-Djava.security.egd=file:/dev/./urandom", "-Dspring.profiles.active=prod", "-jar", "/app.jar"]

EXPOSE 8080
